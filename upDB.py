#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  upDB.py - Updates CTM Radio database using file's metadata, and creates a
#  preview for every song.
#
#  Version 13.0.0
#
#  Copyleft 2012 - 2020 Felipe Peñailillo Castañeda (@breadmaker) <breadmaker@radiognu.org>
#                       Jorge Araya Navarro (@shackra) <elcorreo@deshackra.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse, base64, json, mutagen, os, requests
from mysql import connector
from progressbar import SimpleProgress, Bar, Percentage, Timer, ProgressBar, \
    ETA, ReverseBar
from pydub import AudioSegment
from re import split
from xml.dom.minidom import parseString
from config import DB_USER, DB_PASSWORD, DB_HOST

def getLicenseInfo(url):
    license = next((item for item in licenses if item["url"] == url), None)
    if license == None:
        licenseName = ""
        licenseBaseURL = requests.get("http://api.creativecommons.org/rest/1.5/details",
            params={"license-uri": url, "locale": "en"})
        licenseBaseString = parseString(licenseBaseURL.text.encode("utf-8"))
        for tag in licenseBaseString.getElementsByTagName("license-name"):
            licenseName = "Creative Commons " + tag.firstChild.nodeValue
        license = {"url": url, "name": licenseName,
            "shortname": "CC " + (" ".join(split("/",
                split("licenses", url)[1])[1:-1])).upper()}
        licenses.append(license)
    return {'url': license["url"], 'name': license["name"], 'shortname': license["shortname"]}

cmd_line = argparse.ArgumentParser(description="Keeps the radio's music database updated.")

# adding arguments
cmd_line.add_argument("-d", "--only-database",
                      help="updates only the database",
                      action="store_true")

cmd_line.add_argument("-c", "--clean-covers",
                      help="removes cover art tags",
                      action="store_true")

cmd_line.add_argument('--version',
                      action='version',
                      version='upDB.py 13.0')

arguments = cmd_line.parse_args()

# initializing license cache file
licenses_file = open("licenses.txt", "a+")
licenses_file.seek(0)
try:
    text = licenses_file.read()
    licenses = json.loads(text)
except:
    licenses = []

# here we go!
chest = open("music.txt", "r")
total = sum(1 for line in chest)
chest.seek(0, 0)  # restarting file cursor
conn = connector.connect(user=DB_USER, password=DB_PASSWORD, host=DB_HOST)
c = conn.cursor()
# initializing database, it creates it if it doesn't exist
print("Preparing database...", end='\r')
for line in open("radio.sql", "r"):
    c.execute(line)
print("Database prepared!   ")
current = 0
missing_info = 0
proc_files_widgets = \
    [("" if arguments.only_database else "[1/2] ") + "Processing files: ",
    SimpleProgress("%(value_s)s of %(max_value_s)s"), " ",
    Bar('>'), ' ', Percentage(), ' ', ReverseBar('<'), " ",
    Timer(format="Time: %(elapsed)s"), " ", ETA()]
proc_files_pbar = ProgressBar(widgets=proc_files_widgets, maxval=total).start()
folder_path = None
infofile = None
infojson = None
albuminfo = {}
previews = []
countryID = 0
genreID = 0
albumID = 0
try:
    for file in chest:
        file = file.rstrip("\n")
        current += 1
        proc_files_pbar.update(current)
        song = mutagen.File(file, easy=True)
        if(os.path.dirname(file) != folder_path):
            folder_path = os.path.dirname(file)
            # loading info.json file
            infofile = open(folder_path + "/info.json", "r")
            infojson = infofile.read()
            # temp code for migration from the old country.txt files to the new
            # info.json files
            try:
                infojson = json.loads(infojson)
                albuminfo["country"] = infojson["country"]
                albuminfo["genre"] = infojson["genre"]
                albuminfo["url"] = infojson["url"]
            except:
                albuminfo["country"] = infojson.rstrip("\n")
                albuminfo["genre"] = ""
                albuminfo["url"] = ""
                missing_info += 1
            infofile.close()
            # check and add country
            c.execute("SELECT country_id FROM country WHERE country_name = %(id)s",
                {'id': albuminfo["country"]})
            countryID = c.fetchone()
            if countryID == None:
                c.execute("INSERT INTO country(country_name) VALUES(%(name)s)",
                    {'name': albuminfo["country"]})
                countryID = c.lastrowid
            # check and add genre
            c.execute("SELECT genre_id FROM genre WHERE genre_name = %(id)s",
                {'id': albuminfo["genre"]})
            genreID = c.fetchone()
            if genreID == None:
                c.execute("INSERT INTO genre(genre_name) VALUES(%(name)s)",
                    {'name': albuminfo["genre"]})
                genreID = c.lastrowid
            c.execute("SELECT album_id FROM album WHERE album_path = %(path)s",
                {'path': folder_path})
            albumID = c.fetchone()
            if albumID == None:
                c.execute("INSERT INTO album(album_name, album_path, album_year,"
                    " album_url, album_last_played, genre_id) VALUES(%(name)s,"
                    " %(path)s, %(year)s, %(url)s, %(last_played)s, %(genre)s)",
                    {'name': song["album"][0].encode("utf-8").strip(),
                        'path': folder_path,
                        'year': int(song["date"][0].split("-")[0]),
                        'url': albuminfo["url"],
                        'last_played': "1970-01-01 00:00:00.0",
                        'genre': genreID[0] if isinstance(genreID, tuple) else genreID})
                albumID = c.lastrowid
        else:
            # TODO: If there's something else to do if the first file in the
            # folder has everything we need?
            pass
        # check and add license
        c.execute("SELECT license_id FROM license WHERE license_url = %(url)s",
            {'url': song["license"][0]})
        licenseID = c.fetchone()
        if licenseID == None:
            c.execute("INSERT INTO license(license_url, license_name,"
                " license_shortname) VALUES(%(url)s, %(name)s, %(shortname)s)",
                getLicenseInfo(song["license"][0]))
            licenseID = c.lastrowid
        # check and add artists
        c.execute("SELECT artist_id FROM artist WHERE artist_name = %(name)s",
            {'name': song["artist"][0]})
        artistID = c.fetchone()
        if artistID == None:
            c.execute("INSERT INTO artist(artist_name,"
                " artist_last_played, country_id) VALUES(%(name)s,"
                " %(last_played)s, %(country)s)",
                {'name': song["artist"][0],
                'last_played': "1970-01-01 00:00:00.0",
                'country': countryID[0] if isinstance(countryID, tuple) else countryID})
            artistID = c.lastrowid
        # adding songs
        c.execute("SELECT * FROM song WHERE song_name = %(name)s AND artist_id"
            " = %(artist)s AND album_id = %(album)s", {'name': song["title"][0],
            'artist': artistID[0] if isinstance(artistID, tuple) else artistID,
            'album': albumID[0] if isinstance(albumID, tuple) else albumID})
        if c.fetchone() == None:
            c.execute("INSERT INTO song(song_name, song_path, song_duration,"
                " song_last_played, artist_id, album_id, license_id)"
                " VALUES(%(name)s, %(path)s, %(duration)s, %(last_played)s,"
                " %(artist)s, %(album)s, %(license)s)",
                {'name': song["title"][0], 'path': file,
                'duration': float(song.info.length),
                'last_played': "1970-01-01 00:00:00.0",
                'artist': artistID[0] if isinstance(artistID, tuple) else artistID,
                'album': albumID[0] if isinstance(albumID, tuple) else albumID,
                'license': licenseID[0] if isinstance(licenseID, tuple) else licenseID})
        # checks if cover art needs to be removed
        if arguments.clean_covers:
            if "metadata_block_picture" in song:
                song.pop("metadata_block_picture")
                song.save()
            if "coverart" in song:
                song.pop("coverart")
                song.save()
            if "coverartmime" in song:
                song.pop("coverartmime")
                song.save()
        # checks if preview file for that song already exists
        if not os.path.isfile(file.replace(".ogg", "_preview.ogg")):
            metadata = {}
            # copying metadata to preview file
            for field in song:
                # except the ones who contain cover art
                if field != "metadata_block_picture" and field != "coverart" \
                    and field != "coverartmime":
                    metadata[field] = song[field][0]
            # saving the info needed to generate the preview file
            previews.append({ "file": file, "metadata": metadata })
    proc_files_pbar.finish()
except (KeyboardInterrupt, SystemExit):
    print("Interruption detected, saving progress...")
    licenses_file.seek(0)
    licenses_file.truncate()
    json.dump(licenses, licenses_file)
    licenses_file.close()
    conn.commit()
    raise
chest.close()
# caching licenses
licenses_file.seek(0)
licenses_file.truncate()
json.dump(licenses, licenses_file)
licenses_file.close()
conn.commit()
if arguments.only_database == False:
    if len(previews) > 0:
        print("Database is ready to use!")
        proc_previews_widgets = ["[2/2] Processing previews: ",
            SimpleProgress("%(value_s)s of %(max_value_s)s"), " ",
            Bar('>'), ' ', Percentage(), ' ', ReverseBar('<'), " ",
            Timer(format="Time: %(elapsed)s"), " ", ETA()]
        proc_previews_pbar = ProgressBar(widgets=proc_previews_widgets,
            maxval=len(previews)).start()
        current = 0
        for item in previews:
            current += 1
            proc_previews_pbar.update(current)
            preview = AudioSegment.from_ogg(item["file"])
            # effects are applied depending on the song's length
            if preview.duration_seconds <= 10.0:
                preview = preview
            elif preview.duration_seconds <= 12.0:
                preview = preview[:10000]
                preview = preview.fade_in(750).fade_out(1000)
            else:
                preview = preview[preview.duration_seconds*200:preview.duration_seconds*200+10000]
                preview = preview.fade_in(1000).fade_out(1500)
            # replaygain is applied directly to the file
            preview = preview + (float(song["replaygain_track_gain"][0].split()[0]) + 2.0)
            try:
                # creating preview file
                preview.export(item["file"].replace(".ogg", "_preview.ogg"),
                    "ogg", codec="libvorbis", bitrate="48k", tags=item["metadata"])
            except:
                # TODO: Do something when the preview cannot be saved
                raise
        proc_previews_pbar.finish()
    else:
        print("There's no previews to update!")
print("100% of the files have been processed without errors.")
if missing_info > 0:
    print("There are ", missing_info, " album", "s" if missing_info != 1 else "", " that have an info.json file with incomplete information!", sep='')
