# upDB.py

**_upDB.py_**, is a utility script created exclusively to maintain CTM Radio's music database in a MariaDB database, using Python 3.

## Installation

1. The first thing is to create a virtual environment. In a terminal: `virtualenv -p python3 venv`.
2. Then the virtual environment must be activated: `source venv/bin/activate`.
3. If the required modules have not yet been installed, run: `pip3 install -r requirements.txt`.
4. Finally, run the script: `./upDB.py`.

The script uses the following modules

* mysql-connector
* progressbar2
* mutagen
* pydub

## Permissions and folder structure used by the script

**_upDB_** requires of:

* Having access (either direct or by link) to the music.txt file, which must contain the list of files to process, in the form:

```
/path/to/file/01.ogg
/path/to/file/02.ogg
/path/to/file/03.ogg
...
```

File structure is as follows: Each folder must contain the audio files, a `Cover.png` and a `info.json` files.

* All audio files _should_ be properly named and **must** belong to the same album.
* Every folder **must** contain a `Cover.png` file, which contains the cover of the album in question. The file **must** be 400x400 pixels and be optimized to occupy as little disk space as possible.
* Every folder **must** contain a `info.json` file, which contains additional information about the album in JSON format. The file format **must** be:
    ```json
    {
        "country": "<country name>",
        "genre": "<album overall genre>",
        "url": "<album download url>"
    }
    ```

## Script output

**upDB.py** creates and populates a database with the following structure:

* `album`
    * `album_id`
    * `album_name`
    * `album_path`
    * `album_year`
    * `album_url`
    * `album_last_played`
    * `genre_id` (FK of `genre.genre_id`)
* `artist`
    * `artist_id`
    * `artist_name`
    * `artist_last_played`
    * `country_id` (FK of `country.country_id`)
* `country`
    * `country_id`
    * `country_name`
* `genre`
    * `genre_id`
    * `genre_name`
* `license`
    * `license_id`
    * `license_url`
    * `license_name`
    * `license_shortname`
* `song`
    * `song_id`
    * `song_name`
    * `song_path`
    * `song_duration`
    * `song_last_played`
    * `artist_id` (FK of `artist.artist_id`)
    * `album_id` (FK of `album.album_id`)
    * `license_id` (FK of `license.license_id`)

In addition, the script is responsible for generating a ten-second preview of the song, which is stored in the same folder with the extension `_preview.ogg`.
